# DeepMind Lab Release Notes

## Current Release

### New Features:

1.  Extended functionality of the built-in Tensor Lua library.
2.  Add Image Lua library for loading and scaling PNGs.

### Minor Improvements:

1.  Remove unnecessary dependency of map assets on Lua scripts, preventing
    time-consuming rebuilding of maps when scripts are modified.
2.  Add ability to disable bobbing of reward and goal pickups.

### Bug Fixes:

1.  Increase current score storage from short to long.

## release-2016-12-06 Initial release
